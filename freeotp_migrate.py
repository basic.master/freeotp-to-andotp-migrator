#!/usr/bin/env python3

import base64
import json
import sys
import xml.etree.ElementTree


def read_tokens(filename):
    e = xml.etree.ElementTree.parse(filename).getroot()
    for item in e.findall("string"):
        if item.get("name") == "tokenOrder":
            continue
        token = json.loads(item.text)
        token["secret"] = base64.b32encode(
            bytes(x & 0xff for x in token["secret"])
        ).decode("utf8")

        issuer = token.get("issuerAlt") or \
            token.get("issuerExt") or \
            token.get("issuerInt")
        label = token.get("label") or token.get("labelAlt")

        if label and issuer:
            token_label = "%s - %s" % (issuer, label)
        else:
            token_label = label or issuer

        obj = {
            "algorithm": token["algo"],
            "secret": token["secret"],
            "digits": token["digits"],
            "type": token["type"],
            "label": token_label,
        }

        if token["type"] == "TOTP":
            obj.update({
                "period": token["period"]
            })
        elif token["type"] == "HOTP":
            obj.update({
                "counter": token["counter"]
            })
        else:
            sys.stderr.write("Unknown token type %s - skipping item with label '%s'\n" % (token["type"], token_label))
            continue

        yield obj

def main():
    if sys.version_info.major < 3:
        print("This script requires Python 3.", file=sys.stderr)
        sys.exit(1)

    if len(sys.argv) != 2:
        print("Usage: ./freeotp_migrate.py <filename>", file=sys.stderr)
        sys.exit(1)

    # Dump the tokens.
    output = json.dumps(
        list(read_tokens(sys.argv[1])),
        sort_keys=True,
        indent=2,
        separators=(',', ': ')
    )

    print(output)


if __name__ == "__main__":
    main()
